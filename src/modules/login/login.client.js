import React from 'react';
import {hydrate} from 'react-dom';

import Login from './views/login.view';

hydrate(<Login />, document.getElementById('root'));
