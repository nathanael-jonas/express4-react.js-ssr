// @flow
import type {
  $Request,
  $Response,
  NextFunction,
} from 'express';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Login from './views/login.view';
import {ServerStyleSheet} from 'styled-components';
import path from 'path';

const controllers = {
  getLoginView: (req: $Request, res: $Response, next: NextFunction) => {
    const sheet = new ServerStyleSheet();
    // eslint-disable-next-line max-len
    const body = ReactDOMServer.renderToString(sheet.collectStyles(<Login />));
    const styles = sheet.getStyleTags();
    const file = path.resolve('./public/templates/login.ejs');
    res.render(
        file,
        {title: 'Login', body, styles}
    );
  },
  doLogin: (req: $Request, res: $Response, next: NextFunction) => {

  },
};

export default controllers;
