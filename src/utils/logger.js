const winston = require('winston-namespace');

global.logger = {
  server: winston('server'),
  user: winston('user'),
  socket: winston('socket'),
};
